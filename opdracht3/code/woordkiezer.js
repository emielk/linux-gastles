import * as fs from 'fs';

//Open woordenlijst file and read the content into an array
//Choose 50 random words from the array and put them in a new array
//Print the new array
function main() {    
    var woorden =readFile("woordenlijst-origineel",5000);
    writeFile("woordenlijst",woorden);
    var puzzelWoorden = readFile("woordenlijst",50);

    console.log(puzzelWoorden);
}

//Function to read a file based on variable name
function readFile(fileName, counter) {
    var woordenlijst = fs.readFileSync(fileName, "utf-8").split("\n");
    var woorden = [];
    for (var i = 0; i < counter; i++) {
        var random = Math.floor(Math.random() * woordenlijst.length);
        woorden.push(woordenlijst[random]);
    }

    return woorden;
}

//Function to write a array to a file
function writeFile(fileName, woorden) {
    fs.writeFileSync(fileName, woorden.join("\n"));
}

main();