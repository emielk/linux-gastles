# Linux Gastles

Materiaal gebruikt tijdens gastlessen Linux.

## Commando's

Voor het uitvoeren van opdracht 2, 3 en 4 is een lijst met nuttige commando's opgesteld. Gebruik voor verdere uitleg en opties van het commando **man**

- **wc** (wordcount) Tel het aantal woorden of regels in een file
- **sort** Sorteel de regels in een file
- **grep** Zoek naar een patroon in een file
- **uniq** Maak een lijst van unique waarden in een file
- **cut** Verwijder delen van iedere lijn in een file
- **tr** Verander of verwijder karakters
- **rev** Draai de karakers in een lijn om
- **awk** Patronen zoeken in een file
